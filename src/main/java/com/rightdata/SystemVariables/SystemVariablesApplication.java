package com.rightdata.SystemVariables;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@SpringBootApplication
public class SystemVariablesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemVariablesApplication.class, args);
	}

}
