package com.rightdata.SystemVariables;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {        // Creating a calendar
		return application.sources(SystemVariablesApplication.class);
	}

}
