package com.rightdata.SystemVariables.Repository;

import com.rightdata.SystemVariables.Model.Entity.SystemVariables;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemVariablesRepository extends CrudRepository<SystemVariables, Long> {
}
