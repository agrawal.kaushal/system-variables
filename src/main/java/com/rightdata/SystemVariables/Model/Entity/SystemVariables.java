package com.rightdata.SystemVariables.Model.Entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="system_variables")
public class SystemVariables implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(unique=true, nullable=false)
    private long id;

    @Column(unique=true, nullable=false, name = "variable_id")
    private long variableId;

    @Column(name = "name", length=255)
    private String name;

    @Column(name = "format", length=255)
    private String format;

    @Column(name = "description", length=255)
    private String description;

    public void setId(long id) {
        this.id = id;
    }

    public long getVariableId() {
        return variableId;
    }

    public void setVariableId(long variableId) {
        this.variableId = variableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
