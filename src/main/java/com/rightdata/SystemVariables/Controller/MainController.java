package com.rightdata.SystemVariables.Controller;

import com.rightdata.SystemVariables.Model.DTO.RequestDTO;
import com.rightdata.SystemVariables.Model.DTO.ResponseDTO;
import com.rightdata.SystemVariables.Model.Entity.SystemVariables;
import com.rightdata.SystemVariables.Service.CalendarVariablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/system-variables")
public class MainController {

    @Autowired
    CalendarVariablesService calendarVariablesService;

    public MainController(CalendarVariablesService calendarVariablesService) {
        this.calendarVariablesService = calendarVariablesService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/get-data")
    public ResponseEntity<ResponseDTO> getVariableValue(@RequestBody RequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            String result = calendarVariablesService.getCalendarVariable(requestDTO);
            responseDTO.setFlag(true);
            responseDTO.setResult(result);
        } catch (Exception e) {
            responseDTO.setFlag(false);
            responseDTO.setMessage(e.getCause().getMessage());
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.CONFLICT);
        }
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get-timezones")
    public ResponseEntity<ResponseDTO> getListOfTimeZone() {
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setFlag(true);
        responseDTO.setResult(calendarVariablesService.getListOfTimeZone());
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get-variables")
    public ResponseEntity<List<SystemVariables>> getListOfVariables() {
        List<SystemVariables> result = calendarVariablesService.getAllSystemVariables();
        return new ResponseEntity<List<SystemVariables>>(result, HttpStatus.OK);
    }

}
